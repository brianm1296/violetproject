from main import app
from fastapi.testclient import TestClient
from pydantic import BaseModel
from queries.databasedrinks import DrinksRepository
from typing import Optional

client = TestClient(app)


class DrinkOut(BaseModel):
    id: int
    idDrink: int
    strDrink: str
    strAlcoholic: str


class MockDrinksRepository():
    def get_all(self, filter: Optional[str] = None):
        return [DrinkOut(id=1, idDrink=13244, strDrink="Mocktail", strAlcoholic="Non-Alcoholic")]
    def get_one(self, drink_id: int):
        # For the purpose of this mock repository, we will just return the mock drink if the ID matches
        if drink_id == 1:
            return DrinkOut(id=1, idDrink=13244, strDrink="Mocktail", strAlcoholic="Non-Alcoholic")
        return None

app.dependency_overrides[DrinksRepository] = MockDrinksRepository


def test_get_all_drinks():
    # Arrange
    app.dependency_overrides[DrinksRepository] = MockDrinksRepository

    # Act
    response = client.get("/api/dbdrinks")
    data = response.json()
    print("Returned data:", data)  # This will print the actual returned data

    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert data[0]["id"] == 1
    assert data[0]["idDrink"] == 13244
    assert data[0]["strDrink"] == "Mocktail"
    assert data[0]["strAlcoholic"] == "Non-Alcoholic"



def test_get_one_drink():
    app.dependency_overrides[DrinksRepository] = MockDrinksRepository
    # Act
    response = client.get("/api/dbdrinks/1")
    data = response.json()
    print("Returned data:", data)  # This will print the actual returned data

    # Assert
    assert response.status_code == 200
    assert data["id"] == 1
    assert data["idDrink"] == 13244
    assert data["strDrink"] == "Mocktail"
    assert data["strAlcoholic"] == "Non-Alcoholic"
    app.dependency_overrides = {}
