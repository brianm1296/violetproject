from queries.pool import pool
from models.drinks import Error, DrinkIn, DrinkOut
from typing import List, Optional, Union
from datetime import datetime


class DrinksRepository:
    columns = [
            "idDrink", "strDrink", "strAlcoholic", "strDrinkAlternate",
            "strTags", "strVideo", "strCategory", "strIBA", "strGlass",
            "strInstructions", "strInstructionsES", "strInstructionsDE",
            "strInstructionsFR", "strInstructionsIT", "strInstructionsZH_HANS",
            "strInstructionsZH_HANT", "strDrinkThumb",
            "strIngredient1", "strIngredient2", "strIngredient3",
            "strIngredient4", "strIngredient5", "strIngredient6",
            "strIngredient7", "strIngredient8", "strIngredient9",
            "strIngredient10", "strIngredient11", "strIngredient12",
            "strIngredient13", "strIngredient14", "strIngredient15",
            "strMeasure1", "strMeasure2", "strMeasure3", "strMeasure4",
            "strMeasure5", "strMeasure6", "strMeasure7", "strMeasure8",
            "strMeasure9", "strMeasure10", "strMeasure11", "strMeasure12",
            "strMeasure13", "strMeasure14", "strMeasure15",
            "strImageSource", "strImageAttribution",
            "strCreativeCommonsConfirmed",
            "dateModified"
        ]

    @staticmethod
    def generate_placeholders():
        return ", ".join(["%s"] * len(DrinksRepository.columns))

    @staticmethod
    def extract_values(drink: DrinkIn):
        return [getattr(drink, col, None) for col in DrinksRepository.columns]

    def get_all(self, filter=None) -> Union[Error, List[DrinkOut]]:
        try:
            columns_string = ", ".join(self.columns)
            query = f"SELECT {columns_string} FROM drinks"
            if filter == "NonAlcoholic":
                query += " WHERE strAlcoholic IN ('Non alcoholic', 'Optional alcohol')"
            query += " ORDER BY strDrink DESC"
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(query)
                    return [
                        self.record_to_drink_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all drinks"}

    def create(self, drink: DrinkIn, account_id: int) -> Union[DrinkOut, Error]:
        placeholders = self.generate_placeholders()
        values = self.extract_values(drink)
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:

                    result = db.execute(
                        f"""
                        INSERT INTO drinks
                            ({", ".join(self.columns)})
                        VALUES
                            ({placeholders})
                        RETURNING id;
                        """,
                        values
                    )
                    drink_id = result.fetchone()[0]

                    db.execute(
                        """
                        INSERT INTO account_drinks(account_id, drink_id)
                        VALUES (%s, %s);
                        """,
                        (account_id, drink_id)
                    )

                    conn.commit()
                    return self.drink_in_to_out(drink_id, drink)

        except Exception as e:
            print(e)
            return {"message": "Create drink did not work"}

    def get_one(self, drink_id: int) -> Optional[DrinkOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        f"""
                        SELECT {", ".join(self.columns)}
                        FROM drinks
                        WHERE idDrink = %s
                        """,
                        [drink_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_drink_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that drink"}

    def update(self, drink_id: int, drink: DrinkIn) -> Union[DrinkOut, Error]:
        values = self.extract_values(drink) + [drink_id]
        set_clause = ", ".join([f"{col} = %s" for col in self.columns])

        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        f"""
                        UPDATE drinks
                        SET {set_clause}
                        WHERE idDrink= %s
                        """,
                        values
                    )
                    return self.drink_in_to_out(drink_id, drink)
        except Exception as e:
            print(e)
            return {"message": "Could not update that drink"}

    def delete_one(self, drink_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM drinks
                        WHERE idDrink = %s
                        """,
                        [drink_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_user_saved_drinks(self, account_id):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute("""
                        SELECT drinks.*
                        FROM drinks
                        JOIN account_drinks ON
                               drinks.id = account_drinks.drink_id
                        WHERE account_drinks.account_id = %s;
                    """, (account_id,))
                    return db.fetchall()
        except Exception as e:
            print(e)
            return {"message": "Failed to fetch user saved drinks"}

    def delete_user_drink(self, account_id, drink_id):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute("""
                        DELETE FROM account_drinks
                        WHERE account_id = %s AND drink_id = %s;
                    """, (account_id, drink_id))
                    conn.commit()
        except Exception as e:
            print(e)
            return {"message": "Failed to delete user drink"}

    def drink_in_to_out(self, id: int, drink: DrinkIn):
        old_data = drink.dict()
        return DrinkOut(id=id, **old_data)

    def record_to_drink_out(self, record):
        data = {col: value for col, value in zip(self.columns, record)}
        data["id"] = record[0]  # this line is setting the id equal to iddrink
        if isinstance(data.get("dateModified"), datetime):
            data["dateModified"] = data["dateModified"].isoformat()
        return DrinkOut(**data)
