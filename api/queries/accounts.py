from queries.pool import pool
from models.accounts import AccountIn, AccountOutWithPassword


class AccountQueries:
    def get_account(self, username) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, email, username, hashed_password
                        FROM accounts
                        WHERE username = %s;
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return AccountOutWithPassword(
                        id=int(record[0]),
                        email=record[1],
                        username=record[2],
                        hashed_password=record[3],
                    )
        except Exception as e:
            print(e)
            return {"messsage": "Could not retrieve account"}

    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO accounts (email, username, hashed_password)
                        VALUES (%s, %s, %s)
                        RETURNING id;
                        """,
                        [account.email, account.username, hashed_password],
                    )
                    id = result.fetchone()[0]
                    return AccountOutWithPassword(
                        id=id,
                        email=account.email,
                        username=account.username,
                        hashed_password=hashed_password,
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create account"}
