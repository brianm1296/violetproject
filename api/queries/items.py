from queries.pool import pool
from models.items import Error, ItemIn, ItemOut
from typing import List, Optional, Union


class ItemsRepository:

    def get_all(self) -> Union[Error, List[ItemOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, description
                        FROM items
                        """
                    )
                    return [
                        self.record_to_item_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all items"}


    def create(self, item: ItemIn) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO items
                            (name, description)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [
                            item.name,
                            item.description
                        ]
                    )
                    id = result.fetchone()[0]
                    conn.commit()
                    return self.item_in_to_out(id, item)
        except Exception as e:
            print(e)
            return {"message": "Create item did not work"}


    def get_one(self, item_id: int) -> Optional[ItemOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                             , name
                             , description
                        FROM items
                        WHERE id = %s
                        """,
                        [item_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_item_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that item"}


    def update(self, item_id: int, item: ItemIn) -> Union[ItemOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE items
                        SET name = %s
                          , description = %s
                        WHERE id = %s
                        """,
                        [
                            item.name,
                            item.description,
                            item_id
                        ]
                    )

                    return self.item_in_to_out(item_id, item)
        except Exception as e:
            print(e)
            return {"message": "Could not update that item"}


    def delete_one(self, item_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM items
                        WHERE id = %s
                        """,
                        [item_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False


    def item_in_to_out(self, id: int, item: ItemIn):
        old_data = item.dict()
        return ItemOut(id=id, **old_data)


    def record_to_item_out(self, record):
        return ItemOut(
            id=record[0],
            name=record[1],
            description=record[2],
        )
