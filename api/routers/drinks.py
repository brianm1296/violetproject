from fastapi import APIRouter, HTTPException
import httpx
from dotenv import load_dotenv


load_dotenv()


router = APIRouter()


API_BASE_URL = "https://www.thecocktaildb.com/api/json/v1/1"
TIMEOUT = 10


@router.get("/random")
async def get_random_drink():
    async with httpx.AsyncClient() as client:
        response = await client.get(
            f"{API_BASE_URL}/random.php",
            timeout=TIMEOUT,
        )
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code, detail=response.text
        )

    return response.json()


@router.get("/filter_by_alcoholic/{type}")
async def filter_by_alcoholic(type: str):
    async with httpx.AsyncClient() as client:
        if type == "NonAlcoholic":
            response1 = await client.get(
                f"{API_BASE_URL}/filter.php?a=Non alcoholic",
                timeout=TIMEOUT,
            )
            response2 = await client.get(
                f"{API_BASE_URL}/filter.php?a=Optional alcohol",
                timeout=TIMEOUT,
            )
            if response1.status_code == 200 and response2.status_code == 200:
                merged_results = {
                    "drinks": response1.json()["drinks"] + response2.json()["drinks"]
                }
                return merged_results
            else:
                error_response = response1 if response1.status_code != 200 else response2
                raise HTTPException(
                    status_code=error_response.status_code,
                    detail=error_response.text
                )
        else:
            response = await client.get(
                f"{API_BASE_URL}/filter.php?a={type}",
                timeout=TIMEOUT,
             )
            if response.status_code != 200:
                raise HTTPException(
                    status_code=response.status_code, detail=response.text
                )

            return response.json()


@router.get("/filter_by_glass/{glass_type}")
async def filter_by_glass(glass_type: str):
    async with httpx.AsyncClient() as client:
        response = await client.get(
            f"{API_BASE_URL}/filter.php?g={glass_type}",
            timeout=TIMEOUT,
        )
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code, detail=response.text
        )

    return response.json()


@router.get("/{idDrink}")
async def get_drink_by_id(idDrink: int):
    async with httpx.AsyncClient() as client:
        response = await client.get(
            f"{API_BASE_URL}/lookup.php?i={idDrink}",
            timeout=TIMEOUT,
        )
    if response.status_code != 200:
        raise HTTPException(
            status_code=response.status_code, detail=response.text
        )

    return response.json()
