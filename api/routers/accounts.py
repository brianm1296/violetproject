from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)

from authenticator import authenticator
from queries.accounts import AccountQueries
from models.accounts import (
    AccountIn,
    AccountForm,
    AccountToken,
    DuplicateAccountError,
    Error
)

from pydantic import ValidationError
import logging

logger = logging.getLogger(__name__)

router = APIRouter()


@router.post("/accounts", response_model=AccountToken)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    try:
        logger.debug("Starting account creation...")
        hashed_password = authenticator.hash_password(info.password)
        account = accounts.create(info, hashed_password)
        form = AccountForm(
            email=info.email, username=info.username, password=info.password
        )
        token = await authenticator.login(response, request, form, accounts)
        logger.debug("Account created")
        return AccountToken(account=account, **token.dict())
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="An account with this email already exists.",
        )
    except ValidationError:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Invalid account information. Please check your input",
        )
    except Exception as e:
        logger.error(f"General exception caught: {e}")
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while creating the account.",
        )


@router.get("/token", response_model=AccountToken)
async def get_token(
    request: Request,
    account: AccountIn = Depends(authenticator.try_get_current_account_data),
) -> AccountToken:
    try:
        if account and authenticator.cookie_name in request.cookies:
            return {
                "access_token": request.cookies[authenticator.cookie_name],
                "type": "Bearer",
                "account": account,
            }
        else:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Unauthorized or missing credentials.",
            )
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="An error occurred while processing the token request.",
        )


    # except DuplicateAccountError:
    #     raise HTTPException(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         detail="An account with this email already exists.",
    #     )
    # except ValidationError:
    #     raise HTTPException(
    #         status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
    #         detail="Invalid account information. Please check your input",
    #     )
    # except Exception:
    #     raise HTTPException(
    #         status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    #         detail="An error occurred while creating the account.",
    #     )


# @router.delete("/api/sessions/{account_id}", response_model=bool)
# async def delete_session(
#     account_id: str,
#     account_data: dict = Depends(authenticator.get_current_account_data),
#     session_repo: SessionRepo = Depends(),
# ) -> bool:
#     if "admin" in account_data["roles"]:
#         session_repo.delete_sessions(account_id)
#         return True
#     return False
