from fastapi import APIRouter, Depends, Response
from typing import List, Optional, Union
from queries.items import ItemsRepository
from models.items import Error, ItemIn, ItemOut


router = APIRouter()


@router.get("/", response_model=Union[List[ItemOut], Error])
def get_items(
    repo: ItemsRepository = Depends(),
):
    return repo.get_all()

@router.post("/", response_model=Union[ItemOut, Error])
def create_item(
    item: ItemIn,
    response: Response,
    repo: ItemsRepository = Depends(),
):
    result = repo.create(item)
    if isinstance(result, Error):
        response.status_code = 400
    else:
        response.status_code = 201
    return result

@router.get("/{item_id}", response_model=Optional[ItemOut])
def get_one_item(
    item_id: int,
    response: Response,
    repo: ItemsRepository = Depends(),
) -> ItemOut:
    item = repo.get_one(item_id)
    if item is None:
        response.status_code = 404
    return item

@router.put("/{item_id}", response_model=Union[ItemOut, Error])
def update_item(
    item_id: int,
    item: ItemIn,
    repo: ItemsRepository = Depends(),
) -> Union[Error, ItemOut]:
    return repo.update(item_id, item)

@router.delete("/{item_id}", response_model=bool)
def delete_item(
    item_id: int,
    repo: ItemsRepository = Depends(),
):
    return repo.delete_one(item_id)
