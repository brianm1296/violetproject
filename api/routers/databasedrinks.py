from fastapi import APIRouter, Depends, Response
from typing import List, Optional, Union
from queries.databasedrinks import DrinksRepository
from models.drinks import Error, DrinkIn, DrinkOut, Message
from authenticator import authenticator


router = APIRouter()


@router.get("/", response_model=Union[List[DrinkOut], Error])
def get_all_drinks(
    filter: Optional[str] = None,
    repo: DrinksRepository = Depends(DrinksRepository)
        ):
    return repo.get_all(filter)


@router.delete("/userdrinks/{drink_id}", response_model=Union[Message, Error])
def delete_user_drink(
    drink_id: int,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: DrinksRepository = Depends()
):
    account_id = account_data["id"]
    if repo.delete_user_drink(account_id, drink_id):
        return {"message": "Deleted successfully"}
    else:
        response.status_code = 400


@router.post("/", response_model=Union[DrinkOut, Error])
def create_drink(
    drink: DrinkIn,
    response: Response,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: DrinksRepository = Depends()
):
    account_id = account_data["id"]
    result = repo.create(drink, account_id)
    if isinstance(result, Error):
        response.status_code = 400
    else:
        response.status_code = 201
    return result


@router.get("/{drink_id}", response_model=Optional[DrinkOut])
def get_one_drink(
    drink_id: int,
    response: Response,
    repo: DrinksRepository = Depends(DrinksRepository)
) -> DrinkOut:
    drink = repo.get_one(drink_id)
    if drink is None:
        response.status_code = 404
    return drink


@router.put("/{drink_id}", response_model=Union[DrinkOut, Error])
def update_drink(
    drink_id: int,
    drink: DrinkIn,
    repo: DrinksRepository = Depends()
) -> Union[Error, DrinkOut]:
    return repo.update(drink_id, drink)


@router.delete("/{drink_id}", response_model=bool)
def delete_drink(
    drink_id: int,
    repo: DrinksRepository = Depends()
):
    return repo.delete_one(drink_id)
