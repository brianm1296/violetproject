from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import accounts, items, drinks, databasedrinks


app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(items.router, prefix="/api/items", tags=["Items"])
app.include_router(drinks.router, prefix="/api/drinks", tags=["Drinks"])
app.include_router(databasedrinks.router, prefix="/api/dbdrinks", tags=["dbDrinks"])
# allows a label addition to the fastapi browser

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:5173")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
