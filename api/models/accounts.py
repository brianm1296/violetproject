from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    email: str
    username: str
    password: str


class AccountOut(BaseModel):
    id: int
    email: str
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountForm(BaseModel):
    email: str
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut
