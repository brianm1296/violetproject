from pydantic import BaseModel


class Error(BaseModel):
    message: str


class ItemIn(BaseModel):
    name: str
    description: str


class ItemOut(BaseModel):
    id: int
    name: str
    description: str
