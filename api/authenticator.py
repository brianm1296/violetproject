import os
from fastapi import Depends, HTTPException
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountQueries
from models.accounts import AccountOutWithPassword
from datetime import timedelta


class AppAuthenticator(Authenticator):
    async def get_account_data(
        self,
        email: str,
        accounts: AccountQueries,
    ):
        return accounts.get_account(email)

    def get_account_getter(
        self,
        accounts: AccountQueries = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithPassword):
        try:
            return account.hashed_password
        except KeyError:
            raise HTTPException(status_code=500, detail="Account data is missing 'hashed password'.")


two_hours = timedelta(hours=2)

authenticator = AppAuthenticator(os.environ["SIGNING_KEY"], exp=two_hours)
