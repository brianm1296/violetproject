steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE IF NOT EXISTS account_drinks (
            account_id INT REFERENCES accounts(id) ON DELETE CASCADE,
            drink_id INT REFERENCES drinks(id) ON DELETE CASCADE,
            PRIMARY KEY (account_id, drink_id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE account_drinks;
        """,
    ]
]

# To save a new drink for a user,insert a new row
# into the account_drinks table
# with the user's ID and the drink's ID.
# Can associate multiple drinks
# with a single user and vice versa.
# ON DELETE CASCADE ensures that if an account
# or drink is deleted, all its associations in the
# account_drinks table will also be deleted.
# The PRIMARY KEY constraint on both columns
# ensures that each combination of account_id and
# drink_id is unique, preventing a user from having
# the same drink listed multiple times.
