steps = [
    [

        """
        CREATE TABLE IF NOT EXISTS items (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(1000) NOT NULL,
            description VARCHAR(1000) NOT NULL
        );
        """,

        """
        DROP TABLE items;
        """
    ]
]
