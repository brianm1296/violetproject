import { NavLink, useNavigate } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import useToken from '@galvanize-inc/jwtdown-for-react';


function Nav() {

  const { token, logout } = useToken();
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate("/");
  };

      return (
        <nav style={{color: "#C06014"}} className="navbar navbar-expand-lg bg-body-tertiary">
           <div className="container-fluid">
                              {!token ? (
                                  <>
                                      <NavLink className="text-decoration-none" to="/login">
                                          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                              <li className="nav-link" aria-disabled="true">
                                                  Log In
                                              </li>
                                          </ul>
                                      </NavLink>
                                      <NavLink className="text-decoration-none" to="/signup">
                                          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                              <li className="nav-link" aria-disabled="true">
                                                  Sign Up
                                              </li>
                                          </ul>
                                      </NavLink>
                                  </>
                              ) : (
                                <>
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                        <li className="nav-link" onClick={handleLogout} style={{ cursor: 'pointer' }} aria-disabled="true">
                                            Log Out
                                        </li>
                                    </ul>
                                    <NavLink className="text-decoration-none" to="/drinks">
                                        <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                            <li className="nav-link" aria-disabled="true">
                                                Drinks
                                            </li>
                                        </ul>
                                    </NavLink>

                                    <NavLink className="text-decoration-none" to="/drinks/random">
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                      <li className="nav-link" aria-disabled="true">
                                        Random Drink
                                      </li>
                                    </ul>
                                    </NavLink>

                                    <NavLink className="text-decoration-none" to="/drinks/randomna">
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                      <li className="nav-link" aria-disabled="true">
                                        Random NA Drink
                                      </li>
                                    </ul>
                                    </NavLink>

                                    <NavLink className="text-decoration-none" to="/drinks/byglass">
                                    <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex">
                                      <li className="nav-link" aria-disabled="true">
                                        Drinks By Glass
                                      </li>
                                    </ul>
                                    </NavLink>
                                    </>
)}
            </div>

      </nav>
      );
    }

    export default Nav;
