import React from "react";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { DrinksFilterProvider } from './DrinksFilter';
import './css/app.css';
import Nav from './Nav';
import LoginForm from "./components/Accounts/LoginForm";
import SignupForm from "./components/Accounts/SignupForm";
import MainPage from './MainPage';
import { DrinksHome } from "./components/Drinks/DrinksHome";
import GetRandom from "./components/Drinks/GetRandom";
import RandomNA from "./components/Drinks/RandomNA";
import ByGlass from "./components/Drinks/ByGlass";
import LoggedIn from "./components/Accounts/LoggedIn";


function App() {
	const baseUrl = process.env.REACT_APP_API_HOST;
	return (
		<AuthProvider baseUrl={baseUrl}>
		<DrinksFilterProvider>
			<BrowserRouter>
				<Nav />
				<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="drinks/*" element={<Outlet />}>
					<Route path="" element={<DrinksHome/>} />
					<Route path="random/" element={<GetRandom />} />
					<Route path="randomna/" element={<RandomNA />} />
					<Route path="byglass/" element={<ByGlass />} />
					</Route>
					<Route path="/login" element={<LoginForm/>} />
					<Route path="/signup" element={<SignupForm/>} />
					<Route path="/loggedin" element={<LoggedIn/>} />

				</Routes>
				</div>
			</BrowserRouter>
		</DrinksFilterProvider>
		</AuthProvider>
	);
  }

  export default App;
