import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';


// ReactDOM.render(<App />, document.getElementById('root'));

const root = document.getElementById('root');
const appRoot = createRoot(root);
appRoot.render(<App />);
