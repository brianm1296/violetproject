import React, { useState, useEffect } from 'react';
// import './css/Items.css'

function Items() {
    const [items, setItems] = useState([]);
    const [item, setItem] = useState({ name: "", description: "" });
    const [currentItem, setCurrentItem] = useState(null);


    useEffect(() => {
        fetch("http://localhost:8000/api/items")
            .then(res => res.json())
            .then(data => setItems(data))
            .catch(err => console.error('Error fetching items:', err));
    }, []);

    function handleChange(e) {
        setItem({ ...item, [e.target.name]: e.target.value });
    }


    function handleSubmit(e) {
        e.preventDefault();
        fetch("http://localhost:8000/api/items", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(item),
        })
            .then((res) => res.json())
            .then((data) => {
                setItems([...items, data]);
                setItem({ name: "", description: "" });
            });
    }

    function handleEdit(editItem) {
        setCurrentItem(editItem);
    }

    function handleSave() {
        fetch("http://localhost:8000/api/items", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(currentItem),
        })
        .then((res) => res.json())
        .then((data) => {
            // Replace the old item with the edited one in the items array
            const updatedItems = items.map(item =>
                item.id === currentItem.id ? currentItem : item
            );
            setItems(updatedItems);
            setCurrentItem(null); // Close the edit form after saving
        })
        .catch(err => console.error('Error saving item:', err));
    }

    function handleDelete(deleteItem) {
        const userConfirmed = window.confirm("Are you sure you want to delete this item?");
    if (userConfirmed) {
        fetch(`http://localhost:8000/api/items/${deleteItem.id}`, {
            method: "DELETE"
        })
        .then(() => {
            const updatedItems = items.filter(item => item.id !== deleteItem.id);
            setItems(updatedItems);
        })
        .catch(err => console.error('Error deleting item:', err));
    }}

    return (
        <div>
            <div>
                <h1>Items</h1>
                <table className="items-table">
                    <thead>
                        <tr>
                            <th className="left-align">Name</th>
                            <th className="left-align">Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map((item, index) => (
                            <tr className="item-table-row" key={item.id}>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td>
                                    <button className="edit-button" onClick={() => handleEdit(item)}>Edit</button>
                                    <button onClick={() => handleDelete(item)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>

            {currentItem && (
                    <div>
                        <h2>Edit Item</h2>
                        <label>
                            Name:
                            <input
                                value={currentItem.name}
                                onChange={(e) => setCurrentItem({ ...currentItem, name: e.target.value })}
                            />
                        </label>
                        <label>
                            Description:
                            <input
                                value={currentItem.description}
                                onChange={(e) => setCurrentItem({ ...currentItem, description: e.target.value })}
                            />
                        </label>
                        <button onClick={handleSave}>Save</button>
                    </div>
                )}
            <h2>Add Item</h2>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>
                        Add Items:
                        <input
                            type="text"
                            name="name"
                            required
                            value={item.name}
                            placeholder="Item name"
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <div>
                    <label>
                        Description:
                        <input
                            type="text"
                            name="description"
                            required
                            placeholder="Item description"
                            value={item.description}
                            onChange={handleChange}
                        />
                    </label>
                </div>
                <button type="submit">Add Item</button>
            </form>
        </div>
    );
}

export default Items;
