export const saveDrink = async (drink, token) => {
    try {
        const response = await fetch("http://localhost:8000/api/dbdrinks/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(drink)
        });

        const data = await response.json();

        if (response.ok) {
            return { success: true, data: data };
        } else {
            throw new Error(data.detail || "Error saving drink.");
        }
    } catch (error) {
        return { success: false, error: error.message };
    }
};


export const deleteDrink = async (drink, token) => {
    try {
        const response = await fetch(`http://localhost:8000/api/dbdrinks/userdrinks/${drink.idDrink}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(drink)
        });

        const data = await response.json();

        if (response.ok) {
            return { success: true, data: data };
        } else {
            throw new Error(data.detail || "Error deleting drink.");
        }
    } catch (error) {
        return { success: false, error: error.message };
    }
};
