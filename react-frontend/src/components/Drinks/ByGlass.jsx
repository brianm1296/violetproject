import React, { useState } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import GlassCard from './GlassCard';
import { saveDrink } from './Utils.jsx';
import './test.css';

const glasses = [
    { name: "Brandy snifter", img: "/brandy.jpg" },
    { name: "Hurricane glass", img: "/hurricane.jpg" },
    { name: "Coffee mug", img: "/coffeemug.jpg" },
    { name: "Martini Glass", img: "/martini.png" },
    { name: "Mason jar", img: "/mason.jpg" },
];

const ByGlass = () => {
    const { token } = useAuthContext();
    const [selectedGlass, setSelectedGlass] = useState(null);
    const [randomDrink, setRandomDrink] = useState(null);

    const handleGlassClick = async (glassType) => {
        // Fetch drinks for the selected glass type
        const response = await fetch(`http://localhost:8000/api/drinks/filter_by_glass/${glassType}`);
        const data = await response.json();

        // Pick a random drink from the list of drinks returned
        const randomDrinkSelection = data.drinks[Math.floor(Math.random() * data.drinks.length)];

        // Fetch detailed data for the selected random drink
        const drinkResponse = await fetch(`http://localhost:8000/api/drinks/${randomDrinkSelection.idDrink}`);
        const drinkData = await drinkResponse.json();

        // Set the selected glass type and detailed random drink data to state
        setSelectedGlass(glassType);
        setRandomDrink(drinkData.drinks[0]);  // Use the detailed drink data here

        console.log("Glass clicked:", glassType);
        console.log("Data returned:", data);
        console.log("Random drink selected:", randomDrink);
    };

    const handleSaveDrink = async (drink) => {
        const result = await saveDrink(drink, token);

        if (result.success) {
            console.log("Drink saved successfully!", result.data);
            return true;
        } else {
            console.error("Failed to save drink:", result.error);
            return false;
        }
    };


    return (
        <div className="cards-container">

                {glasses.map((glass, index) => (
                    <div key={index} >
                        {selectedGlass === glass.name && randomDrink ? (
                                    <GlassCard
                                    type="drink"
                                    item={randomDrink}
                                    onClick={() => setSelectedGlass(null)}
                                    onSave={handleSaveDrink}
                                />
                                ) : (
                                <GlassCard
                                type="glass"
                                item={glass}
                                onClick={() => handleGlassClick(glass.name)}
                                />
                            )}
                    </div>
                ))}

        </div>
    );
};

export default ByGlass;
