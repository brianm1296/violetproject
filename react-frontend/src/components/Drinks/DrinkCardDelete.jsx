import React from 'react';
import './test.css';

function DrinkCardDel({ drink, onClick, onDelete  }) {

    const handleDelete = (e) => {
        e.stopPropagation(); // Prevents the onClick
        fetch(`http://localhost:8000/api/dbdrinks/${drink.idDrink}`, {
            method: 'DELETE',
        })
        .then(response => {
            if (response.ok) {
                onDelete(drink.idDrink);
            } else {
                console.error("Failed to delete drink");
            }
        });
    };



    const generateIngredientsTable = () => {
        let ingredients = [];
        for (let i = 1; i <= 15; i++) {
            if (drink[`strIngredient${i}`]) {
                ingredients.push(
                    <tr key={i}>
                        <td>{drink[`strIngredient${i}`]}</td>
                        <td>{drink[`strMeasure${i}`]}</td>
                    </tr>
                );
            }
        }
        return ingredients;
    };


    return (
        <div className="drink-card" onClick={onClick}>
            <h3>{drink.strDrink}</h3>
            <img
                src={drink.strDrinkThumb}
                alt={drink.strDrink}
                width="150"
                height="150"
            />
            <table>
                <thead>
                    <tr>
                        <th>Ingredient</th>
                        <th>Measure</th>
                    </tr>
                </thead>
                <tbody>{generateIngredientsTable()}</tbody>
            </table>
            <p>{drink.strInstructions}</p>
            <button onClick={handleDelete}>Delete</button>
        </div>
    );
}

DrinkCardDel.defaultProps = {
    drinkName: '',
    imgSrc: '',
    onClick: () => {},
    onDelete: () => {}
};

export default DrinkCardDel;
