import React, { useEffect, useState } from "react";
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useDrinksFilter } from '../../DrinksFilter';
import DrinkCardDel from "./DrinkCardDelete";

export function DrinksHome() {
    const { token } = useAuthContext();
    const [drinks, setDrinks] = useState([]);
    const [loading, setLoading] = useState(true);

    const { filterNonAlcoholic, setFilterNonAlcoholic } = useDrinksFilter();

    useEffect(() => {
        async function fetchDrinks() {
            try {
                const endpoint = filterNonAlcoholic
                    ? `http://localhost:8000/api/dbdrinks/?filter=NonAlcoholic`
                    : `http://localhost:8000/api/dbdrinks/`;

                const response = await fetch(endpoint);
                const data = await response.json();
                setDrinks(data);
                setLoading(false);
            } catch (error) {
                console.error("Error fetching drinks:", error);
            }
        }
        fetchDrinks();
    }, [filterNonAlcoholic]);


    const handleDelete = async (deletedDrinkId) => {
        try {
            const response = await fetch(`http://localhost:8000/api/dbdrinks/userdrinks/${deletedDrinkId}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            if (response.ok) {
                const updatedDrinks = drinks.filter(drink => drink.idDrink !== deletedDrinkId);
                setDrinks(updatedDrinks);
            } else {
                console.error("Failed to delete drink:", await response.json());
            }
        } catch (error) {
            console.error("Error deleting drink:", error);
        }
    }

    return (
        <div>
            <h1>Saved Drinks</h1>
            <button onClick={() => setFilterNonAlcoholic(!filterNonAlcoholic)}>
                {filterNonAlcoholic ? "Show All Drinks" : "Show Only Non-Alcoholic Drinks"}
            </button>
            {loading ? (
                <p>Loading...</p>
            ) : (
              <div className="cards-container">
              {drinks.map(drink => <DrinkCardDel key={drink.idDrink} drink={drink} onDelete={handleDelete} />)}
          </div>
            )}
        </div>
    );
}
