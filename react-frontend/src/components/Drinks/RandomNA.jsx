import React, { useState, useEffect } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { saveDrink } from './Utils.jsx';
import DrinkCard from './DrinkCard';
import './test.css';

const RandomNA = () => {
    const { token } = useAuthContext();
    const [randomDrink, setRandomDrink] = useState({ idDrink: '', strDrink: '', strDrinkThumb: '', strInstructions: '' });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    async function fetchRandom() {
        setLoading(true);
        setError(null);

        try {
            const response = await fetch(`http://localhost:8000/api/drinks/filter_by_alcoholic/NonAlcoholic`);
            const data = await response.json();

            const randomDrinkSelection = data.drinks[Math.floor(Math.random() * data.drinks.length)];
            const drinkResponse = await fetch(`http://localhost:8000/api/drinks/${randomDrinkSelection.idDrink}`);
            const drinkData = await drinkResponse.json();

            setRandomDrink(drinkData.drinks[0]);
            setLoading(false);
        } catch (err) {
            setError('Failed to fetch drink.');
            setLoading(false);
        }
    }

    useEffect(() => {
        fetchRandom();
    }, []);  // without this page doesn't load a drink to start

    const handleSaveDrink = async (drink) => {
        const result = await saveDrink(drink, token);

        if (result.success) {
            console.log("Drink saved successfully!", result.data);
            return true;
        } else {
            console.error("Failed to save drink:", result.error);
            return false;
        }
    };

    return (
        <div>
            {loading ? (
                <p>Loading...</p>
            ) : error ? (
                <p>Error: {error}</p>
            ) : (
                <div>
                    <button onClick={fetchRandom}>Random</button>
                    <DrinkCard onSave={handleSaveDrink} drink={randomDrink} />
                </div>
            )}
        </div>
    );
}

export default RandomNA;
