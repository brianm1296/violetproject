import React, { useState } from 'react';
import './test.css';


function DrinkCard({ drink, onClick, onSave, token}) {
    const [isSaved, setIsSaved] = useState(false);
    const handleSave = async (drink) => {
        const result = await onSave(drink, token);
        if (result) {
            setIsSaved(true);
        }
    };

    const generateIngredientsTable = () => {
        let ingredients = [];
        for (let i = 1; i <= 15; i++) {
            if (drink[`strIngredient${i}`]) {
                ingredients.push(
                    <tr key={i}>
                        <td>{drink[`strIngredient${i}`]}</td>
                        <td>{drink[`strMeasure${i}`]}</td>
                    </tr>
                );
            }
        }
        return ingredients;
    };


    return (
        <div className="drink-card" onClick={onClick}>
            <h3>{drink.strDrink}</h3>
            <img
                src={drink.strDrinkThumb}
                alt={drink.strDrink}
                width="150"
                height="150"
            />
            <table>
                <thead>
                    <tr>
                        <th>Ingredient</th>
                        <th>Measure</th>
                    </tr>
                </thead>
                <tbody>{generateIngredientsTable()}</tbody>
            </table>
            <p>{drink.strInstructions}</p>
            {isSaved ? (
                <span>Saved</span>
            ) : (
                <button onClick={(e) => {
                    e.stopPropagation();  // prevent flip
                    handleSave(drink, token);
                }}>
                    Save
                    </button>
            )}
        </div>
    );
}

DrinkCard.defaultProps = {
    drinkName: '',
    imgSrc: '',
    onClick: () => {},
    onSave: () => {},
};

export default DrinkCard;
