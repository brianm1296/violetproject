import React, { useState } from 'react';
import './test.css';


const GlassCard = ({ type, item, onClick, onSave }) => {

    const [isSaved, setIsSaved] = useState(false);

    const handleSave = async (drink) => {
        const result = await onSave(drink);
        if (result) {
            setIsSaved(true);
        }
    };



    const generateIngredientsTable = () => {
        let ingredients = [];
        for (let i = 1; i <= 15; i++) {
            if (item[`strIngredient${i}`]) {
                ingredients.push(
                    <tr key={i}>
                        <td>{item[`strIngredient${i}`]}</td>
                        <td>{item[`strMeasure${i}`]}</td>
                    </tr>
                );
            }
        }
        return ingredients;
    };

    if (type === "drink") {
        return (
            <div className="drink-card" onClick={onClick}>
                <h3>{item.strDrink}</h3>
                <img
                    src={item.strDrinkThumb}
                    alt={item.strDrink}
                    width="150"
                    height="150"
                />
                <table>
                    <thead>
                        <tr>
                            <th>Ingredient</th>
                            <th>Measure</th>
                        </tr>
                    </thead>
                    <tbody>{generateIngredientsTable(item)}</tbody>
                </table>
                <p>{item.strInstructions}</p>
                {isSaved ? (
                    <span>Saved</span>
                ) : (
                    <button onClick={(e) => {
                        e.stopPropagation();  // prevent flip
                        handleSave(item);
                    }}>
                        Save
                    </button>
                )}
            </div>
        );

    } else {

    return (
        <div className="drink-card" onClick={onClick}>
        <img src={item.img} alt={item.name} className="card-img-top" />
        <div>
            <h5>{item.name}</h5>
        </div>
    </div>
    );
}
};

GlassCard.defaultProps = {
    drinkName: '',
    imgSrc: '',
    onClick: () => {},
    onSave: () => {},
};

export default GlassCard;
