import React, { useState, useEffect } from 'react';
import { useAuthContext } from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from 'react-router-dom';
import { saveDrink } from './Utils.jsx';
import DrinkCard from './DrinkCard';

function GetRandom() {
    const { token } = useAuthContext();
    const navigate = useNavigate();
    const [drink, setDrink] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);


    async function fetchRandom() {
        setLoading(true);
        setError(null);
        try {
            const response = await fetch("http://localhost:8000/api/drinks/random");
            if (response.ok) {
                const data = await response.json();
                setDrink(data.drinks[0]);
            } else {
                setError("Failed to fetch a random drink.");
            }
        } catch (error) {
            setError(error.message);
        } finally {
            setLoading(false);
        }
    }

    const handleSaveDrink = async (drink) => {
        const result = await saveDrink(drink, token);

        if (result.success) {
            console.log("Drink saved successfully!", result.data);
            return true;
        } else {
            console.error("Failed to save drink:", result.error);
            return false;
        }
    };

    useEffect(() => {
        fetchRandom();
    }, []);  // Empty dependency array means this useEffect runs once when component mounts.

    return (
        <div>
            {loading ? (
                <p>Loading...</p>
            ) : error ? (
                <p>Error: {error}</p>
            ) : (
                <div>
                    <button onClick={() => fetchRandom()}>Random</button>
                    <DrinkCard onSave={handleSaveDrink} drink={drink} token={token} />
                </div>
            )}
        </div>
    );
}

export default GetRandom;
