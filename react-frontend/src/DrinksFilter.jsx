import React, { createContext, useContext, useState } from 'react';

const DrinksFilterContext = createContext();

export const DrinksFilterProvider = ({ children }) => {
    const [filterNonAlcoholic, setFilterNonAlcoholic] = useState(false);

    return (
        <DrinksFilterContext.Provider value={{ filterNonAlcoholic, setFilterNonAlcoholic }}>
            {children}
        </DrinksFilterContext.Provider>
    );
};

export const useDrinksFilter = () => {
    const context = useContext(DrinksFilterContext);
    if (!context) {
        throw new Error("useDrinksFilter must be used within a DrinksFilterProvider");
    }
    return context;
};
